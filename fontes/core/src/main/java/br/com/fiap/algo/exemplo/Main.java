package br.com.fiap.algo.exemplo;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

import br.com.fiap.algo.core.GerenciadorDeExecucao;
import br.com.fiap.algo.core.SortAlgorithm;
import br.com.fiap.algo.core.SortAlgorithmFactory;

public class Main {

	private static ArrayComponent data = new ArrayComponent();

	public static void main(String[] args) throws Exception {
		Map<String, String> argumentos = parseArgs(args);
		
		if (argumentos.get("metodo").equalsIgnoreCase("list-all")) {
			listaTodosAlgoritmos();
			System.exit(0);
		} 
		
		SortAlgorithm algoritmo = SortAlgorithmFactory.getInstance(argumentos.get("-a"));
		GerenciadorDeExecucao gerenciador = new GerenciadorDeExecucao(argumentos.get("-i"));
		
		setupFrame(data, 300, 300);
		Thread thread = new Thread(new Sorter(gerenciador.getValores(), data, algoritmo));
		thread.start();
		thread.join();
		
		gerenciador.writeData(algoritmo, argumentos.get("-o"));
//		gerenciador.writeData(algoritmo, "C:\\Users\\Marcio\\Downloads\\projeto\\rm43039-rm43503-rm43191-rm43284");
	}

	private static void listaTodosAlgoritmos() {
		System.out.println("quick_sort");
		System.out.println("merge_sort");
		System.out.println("insertion_sort");
		System.out.println("heap_sort");
	}

	static void setupFrame(ArrayComponent data, int width, int height) {
		JFrame frame = new JFrame();
		frame.add(data, BorderLayout.CENTER);
		frame.setSize(width, height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	public static Map<String, String> parseArgs(String[] args){
		Map<String, String> argumentos = new HashMap<String, String>();
		
		for (int i = 0; i < args.length; i++) {
			if (i == 0) {
				argumentos.put("metodo", args[i]);
			} else {

				if ((args[i].isEmpty() || args[i].indexOf("-") != 0) && !"-o".equalsIgnoreCase(args[i])) {
					throw new IllegalArgumentException("Parametro obrigatorio: " + args[i]);
				}
				
				if (args[i+1].isEmpty() || args[i+1].indexOf("-") == 0) {
					throw new IllegalArgumentException("Valor do parametro "+ args[i] +" obrigatorio");
				}
				
				argumentos.put(args[i], args[++i]);
			}   
		}
		
		return argumentos;
	}

	static Double[] readData(int height) throws Exception {
		BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream("input.txt")));
	
		Double[] values = new Double[Integer.parseInt(input.readLine())];
		
		String linha = input.readLine();
		int i = 0;
		while(linha != null) {
			values[i++] = Double.parseDouble(linha);
			linha = input.readLine();
		}

		return values;
	}
}
