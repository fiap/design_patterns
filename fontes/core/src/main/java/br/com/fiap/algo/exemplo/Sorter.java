package br.com.fiap.algo.exemplo;

import br.com.fiap.algo.core.SortAlgorithm;
import br.com.fiap.algo.impl.MergeSort;
import java.util.*;

/**
 * ESSA CLASSE FOI RETIRADA DE UM EXEMPLO DO LIVRO
 * "OO Design & Patterns, 2nd ed".
 * 
 */
public class Sorter implements Runnable {

	private Double[] data;
	private ArrayComponent panel;
	private final SortAlgorithm metodoSort;

	public Sorter(Double[] values, ArrayComponent panel, SortAlgorithm metodoSort) {
		this.data = values;
		this.panel = panel;
		this.metodoSort = metodoSort;
	}

	public void run() {
		Comparator<Double> comp = new Comparator<Double>() {

			public int compare(Double d1, Double d2) {
				panel.setValues(data, d1, d2);
				try {
					Thread.sleep(50);
				} catch (InterruptedException exception) {
					Thread.currentThread().interrupt();
				}
				return (d1).compareTo(d2);
			}
		};

		metodoSort.sort(data, comp);
		panel.setValues(data, null, null);
	}
}