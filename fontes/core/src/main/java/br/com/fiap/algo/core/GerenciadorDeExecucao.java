package br.com.fiap.algo.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class GerenciadorDeExecucao {
	private final String arquivoDeEntrada;
	private Double[] valores;

	public GerenciadorDeExecucao(String caminhoArquivoDeEntrada) {
		this.arquivoDeEntrada = caminhoArquivoDeEntrada;
		readData();
	}
	
	private void readData(){
		BufferedReader input;
		try {
			input = new BufferedReader(new InputStreamReader(new FileInputStream(this.arquivoDeEntrada)));
			valores = new Double[Integer.parseInt(input.readLine())];
			
			String linha = input.readLine();
			int i = 0;
			while(linha != null) {
				valores[i++] = Double.parseDouble(linha);
				linha = input.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void writeData(SortAlgorithm metodoSort, String caminhoArquivoDeSaida){
		if (caminhoArquivoDeSaida == null || caminhoArquivoDeSaida.isEmpty()) {
			caminhoArquivoDeSaida = "."; 
		}
		
		caminhoArquivoDeSaida += "/";
		
		File arquivo = new File(caminhoArquivoDeSaida + metodoSort.getAlias()+".txt");  
        FileOutputStream fos;
		try {
			fos = new FileOutputStream(arquivo);
			fos.write((valores.length+"\r\n").getBytes());  
			fos.write((metodoSort.getQuantidadeComparacoes() + " " + metodoSort.getTempoExecucao()+"\r\n").getBytes());
			for (int i = 0; i < valores.length; i++) {
				fos.write((valores[i].intValue() + "\r\n").getBytes());
			}
			fos.close();  
		} catch (IOException e) {
			
		} 
	}

	public String getArquivo() {
		return arquivoDeEntrada;
	}

	public Double[] getValores() {
		return valores;
	}
}
