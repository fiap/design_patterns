package br.com.fiap.algo.core;

import br.com.fiap.algo.impl.HeapSort;
import br.com.fiap.algo.impl.InsertionSort;
import br.com.fiap.algo.impl.MergeSort;
import br.com.fiap.algo.impl.QuickSort;

public class SortAlgorithmFactory {

	public static SortAlgorithm getInstance(String alias){
		if ("merge_sort".equalsIgnoreCase(alias)) {
			return new MergeSort();
		} else if ("quick_sort".equalsIgnoreCase(alias)) {
			return new QuickSort();
		} else if ("insertion_sort".equalsIgnoreCase(alias)) {
			return new InsertionSort();
		} else if ("heap_sort".equalsIgnoreCase(alias)) {
			return new HeapSort();
		} else {
			throw new RuntimeException("Algoritmo inválido.");
		}
	}
}
