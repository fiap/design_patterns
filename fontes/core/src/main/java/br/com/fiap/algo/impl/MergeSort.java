package br.com.fiap.algo.impl;

import br.com.fiap.algo.core.SortAlgorithm;
import java.util.*;

import org.joda.time.DateTime;

/**
 * Implementação MergeSort
 */
public class MergeSort implements SortAlgorithm {

	private long tempoExecucao;
	private long quantidadeComparacoes;

	@Override
	public <E> void sort(E[] a, Comparator<? super E> comp) {
		DateTime inicio = new DateTime();
		
		mergeSort(a, 0, a.length - 1, comp);
		
		DateTime fim = new DateTime();
		tempoExecucao = fim.getMillis() - inicio.getMillis();
	}

	@Override
	public String getAlias() {
		return "merge_sort";
	}

	private <E> void mergeSort(E[] a, int from, int to, Comparator<? super E> comp) {
		if (from == to) {
			return;
		}
		
		int mid = (from + to) / 2;
		// Sort the first and the second half
		mergeSort(a, from, mid, comp);
		mergeSort(a, mid + 1, to, comp);
		merge(a, from, mid, to, comp);
	}

	private <E> void merge(E[] a, int from, int mid, int to, Comparator<? super E> comp) {
		int n = to - from + 1;

		Object[] b = new Object[n];

		int i1 = from;
		int i2 = mid + 1;
		int j = 0;

		while (i1 <= mid && i2 <= to) {
			quantidadeComparacoes++;
			if (comp.compare(a[i1], a[i2]) < 0) {
				b[j] = a[i1];
				i1++;
			} else {
				b[j] = a[i2];
				i2++;
			}
			j++;
		}

		while (i1 <= mid) {
			b[j] = a[i1];
			i1++;
			j++;
		}

		while (i2 <= to) {
			b[j] = a[i2];
			i2++;
			j++;
		}

		for (j = 0; j < n; j++) {
			a[from + j] = (E) b[j];
		}
	}

	@Override
	public Long getTempoExecucao() {
		return tempoExecucao;
	}

	@Override
	public Long getQuantidadeComparacoes() {
		return quantidadeComparacoes;
	}
}
