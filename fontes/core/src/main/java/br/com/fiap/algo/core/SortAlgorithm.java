package br.com.fiap.algo.core;

import java.util.Comparator;

public interface SortAlgorithm {

    String getAlias();

    <E> void sort(E[] a, Comparator<? super E> comp);
    
    Long getTempoExecucao();
    Long getQuantidadeComparacoes();
}
