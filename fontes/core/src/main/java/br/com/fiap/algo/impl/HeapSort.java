package br.com.fiap.algo.impl;

import java.util.Comparator;

import org.joda.time.DateTime;

import br.com.fiap.algo.core.SortAlgorithm;

public class HeapSort implements SortAlgorithm {

	private long tempoExecucao;
	private long quantidadeComparacoes;

	@Override
	public String getAlias() {
		return "heap_sort";
	}

	@Override
	public <E> void sort(E[] a, Comparator<? super E> comp) {
		DateTime inicio = new DateTime();

		heapSort(a, comp);
		
		DateTime fim = new DateTime();
		tempoExecucao = fim.getMillis() - inicio.getMillis();
	}

	private <E> void heapSort(E[] a, Comparator<? super E> comp) {
		for (int i = a.length / 2; i >= 0; i--)
			percDown(a, i, a.length, comp);
		for (int i = a.length - 1; i > 0; i--) {
			swapReferences(a, 0, i);
			percDown(a, 0, i, comp);
		}
	}

	private static int leftChild(int i) {
		return 2 * i + 1;
	}

	private <E> void percDown(E[] a, int i, int n, Comparator<? super E> comp) {
		int child;
		E tmp;

		for (tmp = a[i]; leftChild(i) < n; i = child) {
			child = leftChild(i);
			quantidadeComparacoes++;
			if (child != n - 1 && comp.compare(a[child], a[child + 1]) < 0)
				child++;
			quantidadeComparacoes++;
			if (comp.compare(tmp, a[child]) < 0)
				a[i] = a[child];
			else
				break;
		}
		a[i] = tmp;
	}

	private <E> void swapReferences(E[] a, int index1, int index2) {
		E tmp = a[index1];
		a[index1] = a[index2];
		a[index2] = tmp;
	}

	@Override
	public Long getTempoExecucao() {
		return tempoExecucao;
	}

	@Override
	public Long getQuantidadeComparacoes() {
		return quantidadeComparacoes;
	}
}
