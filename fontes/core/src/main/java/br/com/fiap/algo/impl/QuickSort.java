package br.com.fiap.algo.impl;

import java.util.Comparator;

import org.joda.time.DateTime;

import br.com.fiap.algo.core.SortAlgorithm;

public class QuickSort implements SortAlgorithm {

	private long tempoExecucao;
	private long quantidadeComparacoes;

	@Override
	public String getAlias() {
		return "quick_sort";
	}

	@Override
	public <E> void sort(E[] a, Comparator<? super E> comp) {
		DateTime inicio = new DateTime();

		quickSort(a, 0, a.length - 1, comp);
		
		DateTime fim = new DateTime();
		tempoExecucao = fim.getMillis() - inicio.getMillis();
	}

	private <E> void quickSort(E[] a, int p, int u, Comparator<? super E> comp) {

		int meio;

		if (p < u) {
			meio = partition(a, p, u, comp);
			quickSort(a, p, meio, comp);
			quickSort(a, meio + 1, u, comp);
		}
	}

	public <E> int partition(E[] a, int p, int u, Comparator<? super E> comp) {
		E pivo;
		int topo, i;
		pivo = a[p];
		topo = p;

		for (i = p + 1; i <= u; i++) {
			quantidadeComparacoes++;
			if (comp.compare(a[i], pivo) < 0) {
				a[topo] = a[i];
				a[i] = a[topo + 1];
				topo++;
			}
		}
		a[topo] = pivo;
		return topo;
	}

	@Override
	public Long getTempoExecucao() {
		return tempoExecucao;
	}

	@Override
	public Long getQuantidadeComparacoes() {
		return quantidadeComparacoes;
	}
}
