package br.com.fiap.algo.impl;

import java.util.Comparator;

import org.joda.time.DateTime;

import br.com.fiap.algo.core.SortAlgorithm;

public class InsertionSort implements SortAlgorithm {

	private long tempoExecucao;
	private long quantidadeComparacoes;

	@Override
	public String getAlias() {
		return "insertion_sort";
	}

	@Override
	public <E> void sort(E[] a, Comparator<? super E> comp) {
		DateTime inicio = new DateTime();

		insertionSort(a, comp);
		
		DateTime fim = new DateTime();
		tempoExecucao = fim.getMillis() - inicio.getMillis();
	}
	
	private <E> void insertionSort(E[] a, Comparator<? super E> comp)
    {
        for( int p = 1; p < a.length; p++ )
        {
            E tmp = a[p];
            int j = p;

            for( ; j > 0 && comp.compare(tmp, a[j-1]) < 0; j-- ){
    			quantidadeComparacoes++;
                a[ j ] = a[ j - 1 ];
            }
            a[ j ] = tmp;
        }
    }

	@Override
	public Long getTempoExecucao() {
		return tempoExecucao;
	}

	@Override
	public Long getQuantidadeComparacoes() {
		return quantidadeComparacoes;
	}
}
